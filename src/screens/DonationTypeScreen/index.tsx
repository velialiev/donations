import React, { FC } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import theme from '../../configs/theme'
import { NavigationScreenProp } from 'react-navigation'

const arrowDownIcon = require('../../assets/icons/arrowDownIcon.png')

const arrowRightIconImage = (
  <Image
    width={14}
    height={9.5}
    style={{ width: 14, height: 9.5, transform: [{ rotate: '-90deg' }] }}
    source={arrowDownIcon}
  />
)

const DonationTypeScreen: FC<Props> = ({ navigation }) => {
  return (
    <View style={{ ...styles.screenWrap, ...theme.defaultScreenStyles }}>
      <View style={styles.contentWrap}>
        <TouchableOpacity
          style={styles.linkCard}
          onPress={() => navigation.navigate('TargetDonationScreen')}
        >
          <View>
            <Text style={styles.title}>Целевой сбор</Text>
            <Text style={styles.description}>Когда есть определённая цель</Text>
          </View>
          {arrowRightIconImage}
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.linkCard}
          onPress={() => navigation.navigate('RegularDonationScreen')}
        >
          <View>
            <Text style={styles.title}>Регулярный сбор</Text>
            <Text style={styles.description}>Если помощь нужна ежемесячно</Text>
          </View>
          {arrowRightIconImage}
        </TouchableOpacity>
      </View>
    </View>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
}

const styles = StyleSheet.create({
  screenWrap: {
    padding: theme.screenHorizontalPadding,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentWrap: {
    width: '100%',
    marginBottom: theme.titleBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  linkCard: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 12,
    paddingHorizontal: 16,
    backgroundColor: '#F5F5F5',
    borderRadius: theme.borderRadius,
    marginBottom: 8,
  },
  title: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: theme.semiBoldFontWeight,
    marginBottom: 3,
  },
  description: {
    fontSize: 13,
    lineHeight: 16,
    color: theme.grayColor,
  },
})

export default DonationTypeScreen
