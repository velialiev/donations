import React, { FC } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native'
import theme from '../../configs/theme'
import { Formik } from 'formik'
import ImageUploader from '../../components/ImageUploader'
import Input from '../../components/Input'
import Select from '../../components/Select'
import { NavigationScreenProp } from 'react-navigation'
import Button from '../../components/Button'

const fakeData = [
  { id: 1, title: 'Счёт VK Pay · 1234' },
  { id: 2, title: 'Счёт Сбербанк · 100000' },
]

const TargetDonationScreen: FC<Props> = ({ navigation }) => {
  return (
    <Formik
      initialValues={{
        image: undefined,
        name: '',
        amount: '',
        target: '',
        description: '',
        account: '',
      }}
      onSubmit={(values) => {
        navigation.navigate('PostCreationScreen', values)
      }}
    >
      {({ handleSubmit }) => (
        <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
          <ScrollView>
            <ImageUploader style={styles.formElement} name="image" />
            <Input
              style={styles.formElement}
              label="Название сбора"
              placeholder="Название сбора"
              name="name"
            />
            <Input
              style={styles.formElement}
              label="Сумма, ₽"
              placeholder="Сколько нужно собрать?"
              name="amount"
            />
            <Input
              style={styles.formElement}
              label="Цель"
              placeholder="Например, лечение человека"
              name="target"
            />
            <Input
              style={styles.formElement}
              label="Описание"
              placeholder="На что пойдут деньги и как они кому-то помогут?"
              name="description"
              multiline={true}
            />
            <Select
              style={styles.formElement}
              modalTitle="Куда получать деньги"
              label="Куда получать деньги"
              data={fakeData}
              resolveId={(item) => item.id}
              resolveTitle={(item) => item.title}
              name="account"
              navigation={navigation}
              placeholder="Счёт"
            />

            <Button
              onPress={(e) => {
                handleSubmit(e)
              }}
              style={styles.button}>
              Далее
            </Button>
          </ScrollView>
        </View>
      )}
    </Formik>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
}

const styles = StyleSheet.create({
  screenWrap: {
    paddingHorizontal: theme.screenHorizontalPadding,
    paddingBottom: 50,
  },
  formElement: {
    marginBottom: 26,
  },
  button: {
    width: '100%',
    paddingVertical: 11,
  },
})

export default TargetDonationScreen
