import React, { FC, useEffect } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'
import theme from '../../configs/theme'
import Button from '../../components/Button'
import Select from '../../components/Select'

const fakeData = [
  {
    id: 123,
    text: 'Test',
  },
  {
    id: 213123,
    text: 'Woohoo!',
  },
]

const DonationsScreen: FC<Props> = ({ navigation }) => {
  return (
    <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
      <View style={styles.contentWrap}>
        <View style={styles.textWrap}>
          <Text style={styles.text}>У Вас пока нет сборов.</Text>
          <Text style={styles.text}>Начните доброе дело.</Text>
        </View>

        <View>
          <Button onPress={() => navigation.navigate('DonationTypeScreen')}>Создать сбор</Button>
        </View>
      </View>
    </View>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
}

const styles = StyleSheet.create({
  screenWrap: {
    paddingHorizontal: theme.screenHorizontalPadding,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentWrap: {
    marginBottom: theme.titleBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textWrap: {
    marginBottom: 24,
  },
  text: {
    color: theme.mediumGrayColor,
    fontSize: 16,
  },
})

export default DonationsScreen
