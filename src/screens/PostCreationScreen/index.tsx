import React, { FC } from 'react'
import { StyleSheet, TextInput, View } from 'react-native'
import theme from '../../configs/theme'
import { NavigationScreenProp } from 'react-navigation'
import Snippet from '../../components/Snippet'

export interface Donation {
  image: string | undefined
  name: string
  amount: string
  target: string
  description: string
  account: string
  authorId: number
}

const PostCreationScreen: FC<Props> = ({ navigation, route }) => {
  const donation = route.params as Donation

  return (
    <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
      <TextInput style={styles.description} placeholder="Введите описание" multiline={true} />
      <Snippet
        title={donation.name}
        authorId={donation.authorId}
        description={donation.description}
        imageUri={donation.image}
      />
    </View>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
  route: any
}

const styles = StyleSheet.create({
  screenWrap: {
    paddingHorizontal: theme.screenHorizontalPadding,
  },
  input: {
    fontSize: 15,
  },
  description: {
    marginBottom: 12,
  },
})

export default PostCreationScreen
