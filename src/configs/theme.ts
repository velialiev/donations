const theme = {
  blueColor: '#4986CC',
  lightBlueColor: '#3F8AE0',
  lightGrayColor: '#B8C1CC',
  mediumGrayColor: '#818C99',
  grayColor: '#6D7885',
  darkColor: '#000',
  lightColor: '#fff',
  borderRadius: 10,
  mediumFontWeight: '600' as FontWeight,
  semiBoldFontWeight: '700' as FontWeight,
  defaultScreenStyles: {
    backgroundColor: '#fff',
    height: '100%',
  },
  titleBarHeight: 92,
  screenHorizontalPadding: 12,
}

type FontWeight =
  | 'normal'
  | 'bold'
  | '100'
  | '200'
  | '300'
  | '400'
  | '500'
  | '600'
  | '700'
  | '800'
  | '900'

export default theme
