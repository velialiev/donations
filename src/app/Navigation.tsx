import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ModalScreen from '../screens/ModalScreen'
import DonationsScreen from '../screens/DonationsScreen'
import { NavigationContainer } from '@react-navigation/native'
import DonationTypeScreen from '../screens/DonationTypeScreen'
import TargetDonationScreen from '../screens/TargetDonationScreen'
import RegularDonationScreen from '../screens/RegularDonationScreen'
import PostCreationScreen from '../screens/PostCreationScreen'

const DonationsStack = createStackNavigator()

const MainStackComponent = () => {
  return (
    <DonationsStack.Navigator initialRouteName="DonationsScreen">
      <DonationsStack.Screen
        name="DonationsScreen"
        options={{ title: 'Пожертвования' }}
        component={DonationsScreen}
      />

      <DonationsStack.Screen
        name="DonationTypeScreen"
        options={{ title: 'Тип сбора' }}
        component={DonationTypeScreen}
      />

      <DonationsStack.Screen
        name="TargetDonationScreen"
        options={{ title: 'Целевой сбор' }}
        component={TargetDonationScreen}
      />

      <DonationsStack.Screen
        name="RegularDonationScreen"
        options={{ title: 'Регулярный сбор' }}
        component={RegularDonationScreen}
      />

      <DonationsStack.Screen
        name="PostCreationScreen"
        options={{ title: 'Создание публикации' }}
        component={PostCreationScreen}
      />

      <DonationsStack.Screen
        name="ModalScreen"
        component={ModalScreen}
        options={({ route }: any) => ({
          title: route.params?.headerTitle,
          headerBackTitle: route.params?.headerBackTitle,
        })}
      />
    </DonationsStack.Navigator>
  )
}

const Navigation = () => {
  return (
    <NavigationContainer>
      <MainStackComponent />
    </NavigationContainer>
  )
}

export default Navigation
