import React, { FC, useState } from 'react'
import { StyleSheet, View, Image, Text, TouchableOpacity, ViewStyle } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import theme from '../../configs/theme'
import { ImagePickerResponse } from 'react-native-image-picker/src/internal/types'
import { useField } from 'formik'

const backgroundIcon = require('../../assets/icons/backgroundIcon.png')
const crossIcon = require('../../assets/icons/crossIcon.png')

const imageUploaderOptions = {
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
  allowsEditing: true,
  chooseFromLibraryButtonTitle: 'Выбрать из галерии',
  takePhotoButtonTitle: 'Сделать фотографию',
  cancelButtonTitle: 'Отмена',
  mediaType: 'photo' as MediaType,
}

type MediaType = 'photo' | 'video' | 'mixed'

const ImageUploader: FC<Props> = ({ name, label = 'Загрузить изображение', style }) => {
  const [field, , helper] = useField(name)

  const chooseImage = () => {
    ImagePicker.showImagePicker(
      {
        ...imageUploaderOptions,
        title: label,
      },
      (res) => {
        if (!res.uri) {
          return
        }

        helper.setValue(res.uri)
      },
    )
  }

  const removePreview = () => {
    helper.setValue(undefined)
  }

  return (
    <View style={style}>
      {field.value && (
        <TouchableOpacity style={styles.crossIconWrap} onPress={removePreview}>
          <Image style={styles.crossIcon} width={10} height={10} source={crossIcon} />
        </TouchableOpacity>
      )}

      <TouchableOpacity
        onPress={chooseImage}
        style={{
          ...styles.imageUpload,
          borderColor: !field.value ? theme.lightBlueColor : 'transparent',
        }}
      >
        {field.value && (
          <Image resizeMethod="resize" style={styles.preview} source={{ uri: field.value }} />
        )}
        {!field.value && (
          <View style={styles.labelWrap}>
            <Image style={styles.backgroundIcon} width={25} height={25} source={backgroundIcon} />
            <Text style={styles.radioLabel}>{label}</Text>
          </View>
        )}
      </TouchableOpacity>
    </View>
  )
}

interface Props {
  name: string
  label?: string
  style?: ViewStyle
}

const styles = StyleSheet.create({
  imageUpload: {
    borderWidth: 1,
    borderStyle: 'dashed',
    height: 140,
    borderRadius: theme.borderRadius,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundIcon: {
    width: 25,
    height: 25,
    marginRight: 11,
  },
  labelWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  radioLabel: {
    color: theme.lightBlueColor,
    fontWeight: theme.mediumFontWeight,
  },
  preview: {
    width: '100%',
    height: '100%',
    borderRadius: theme.borderRadius,
  },
  crossIconWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 2,
    width: 24,
    height: 24,
    borderWidth: 2,
    borderColor: theme.lightColor,
    backgroundColor: '#99A2AD',
    borderRadius: 100,
    top: 8,
    right: 8,
  },
  crossIcon: {
    width: 10,
    height: 10,
  },
})

export default ImageUploader
