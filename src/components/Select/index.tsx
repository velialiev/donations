import React, { PropsWithChildren, useMemo } from 'react'
import Input from '../Input'
import { FlatList, Image, StyleSheet, TouchableOpacity, View, Text, TouchableHighlight, ViewStyle } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'
import { useField } from 'formik'

const arrowDownIcon = require('../../assets/icons/arrowDownIcon.png')

const arrowDownIconImage = (
  <Image width={14} height={9.5} style={{ width: 14, height: 9.5 }} source={arrowDownIcon} />
)

const Select = <T,>({
  name,
  label,
  data,
  resolveId,
  resolveTitle,
  modalTitle,
  navigation,
  style,
  placeholder,
}: PropsWithChildren<Props<T>>) => {
  const [field, , helper] = useField(name)

  const openModal = () => {
    navigation.navigate('ModalScreen', {
      headerTitle: modalTitle,
      headerBackTitle: 'Назад',
      renderContent: ({ close }: any) => (
        <FlatList
          data={data}
          renderItem={(item) => {
            const i = item.item as T

            return (
              <TouchableHighlight
                underlayColor={'rgba(0, 0, 0, 0.12)'}
                style={styles.item}
                onPress={() => {
                  helper.setValue(resolveId(i))
                  close()
                }}
              >
                <Text style={styles.itemText}>{resolveTitle(i)}</Text>
              </TouchableHighlight>
            )
          }}
        />
      ),
    })
  }

  const value = useMemo(() => {
    const item = data.find((i) => resolveId(i) === field.value)

    if (!item) {
      return ''
    }

    return resolveTitle(item)
  }, [data, field.value])

  return (
    <>
      <TouchableOpacity style={style} onPress={openModal}>
        <View pointerEvents="none">
          <Input renderValue={() => value} name={name} label={label} placeholder={placeholder} suffix={arrowDownIconImage} />
        </View>
      </TouchableOpacity>
    </>
  )
}

interface Props<T> {
  label: string
  placeholder?: string
  data: T[]
  name: string
  modalTitle: string
  resolveId: (item: T) => string | number
  resolveTitle: (item: T) => string
  navigation: NavigationScreenProp<any, any>
  style?: ViewStyle
}

const styles = StyleSheet.create({
  item: {
    paddingVertical: 11,
    paddingHorizontal: 12,
  },
  itemText: {
    fontSize: 16,
  },
})

export default Select
