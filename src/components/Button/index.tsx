import React, { FC } from 'react'
import { StyleSheet, TouchableOpacity, Text, TouchableOpacityProps } from 'react-native'
import theme from '../../configs/theme'

const Button: FC<Props> = ({ children, ...props }) => {
  return (
    <TouchableOpacity {...props} style={{ ...styles.button, ...(props.style as any), }}>
      <Text style={styles.buttonText}>{children}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    alignSelf: 'flex-start',
    paddingHorizontal: 16,
    paddingVertical: 8,
    backgroundColor: theme.blueColor,
    borderRadius: theme.borderRadius,
  },
  buttonText: {
    fontWeight: theme.mediumFontWeight,
    fontSize: 15,
    color: theme.lightColor,
    textAlign: 'center',
  },
})

interface Props extends TouchableOpacityProps {}

export default Button
