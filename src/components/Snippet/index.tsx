import React, { FC, useMemo } from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import { fakeUsers } from '../../screens/RegularDonationScreen'
import theme from '../../configs/theme'

const Snippet: FC<Props> = ({ imageUri, title, authorId = 1, description }) => {
  const author = useMemo(() => {
    return fakeUsers.find((user) => user.id === authorId)
  }, [fakeUsers, authorId])
  return (
    <View style={styles.snippet}>
      {imageUri && <Image style={styles.snippetImage} source={{ uri: imageUri }} />}
      <View style={styles.content}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.description}>
          {author?.title} · {description}
        </Text>
      </View>
    </View>
  )
}

interface Props {
  imageUri: string | undefined
  title: string
  authorId: number
  description: string
}

const styles = StyleSheet.create({
  snippet: {
    borderRadius: theme.borderRadius,
    borderWidth: 0.5,
    borderColor: '#ebebeb',
  },
  snippetImage: {
    height: 140,
    borderTopLeftRadius: theme.borderRadius,
    borderTopRightRadius: theme.borderRadius,
  },
  title: {
    fontSize: 15,
    fontWeight: theme.semiBoldFontWeight,
  },
  description: {
    fontSize: 16,
    color: theme.mediumGrayColor,
  },
  content: {
    paddingHorizontal: 12,
    paddingVertical: 8,
  },
})

export default Snippet
